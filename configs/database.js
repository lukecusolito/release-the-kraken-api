var Mongoose = require('mongoose');
var Config = require('./config');

Mongoose.Promise = require('bluebird');

var connectionString = 'mongodb://' + Config.database.username + ':' + Config.database.password + '@' + Config.database.host + ':' + Config.database.port + '/' + Config.database.db;
if (Config.database.useLocal) {
  connectionString = 'mongodb://' + Config.database.host + '/' + Config.database.db;
}

Mongoose.connect(connectionString, {
  useMongoClient: true,
});

var db = Mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function callback() {
  console.log("Connection with database succeeded.");
});

exports.Mongoose = Mongoose;
exports.db = db;