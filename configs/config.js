var uuid = require('node-uuid');

module.exports = {
    server: {
        useUrlSettings: true, // Use when debugging
        host: 'localhost',
        port: 8000,
        jwtTokenIssuer: 'http://localhost:8000',
        jwtSecretKey: uuid.v4()
    },
    database: {
        //host: 'ds032319.mlab.com',
        host: 'localhost',
        //port: 32319,
        port: 27017,
        db: 'fknshwow',
        username: 'fknshwow',
        password: 'PasswordGoesHere',
        useLocal: true
    }
};