'use strict';

const Hapi = require('hapi');
const Db = require('./configs/database');
const Config = require('./configs/config');

var host = process.env.virtualDirPath;
var port = process.env.PORT;

if (Config.server.useUrlSettings) {
    host = Config.server.host;
    port = Config.server.port;
}

// Create server
const server = new Hapi.Server();
server.connection({
    host: host,
    port: port,
    routes: {
        cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
    }
});

var validateToken = function (decoded, request, callback) {

    // do your checks to see if the person is valid
    //if (!people[decoded.id]) {
    //    return callback(null, false);
    //}
    //else {
    return callback(null, true);
    //}
};


server.register(require('hapi-auth-jwt2'), function (err) {
    if (err) {
        console.log(err);
    }
    server.auth.strategy('jwt', 'jwt', {
        key: Config.server.jwtSecretKey,
        validateFunc: validateToken,
        verifyOptions: { algorithms: ['HS256'] }
    });
    server.auth.default('jwt');

    // Define routes
    var DiagnosticsRoute = require('./routes/diagnosticsRoute');
    var SetlistRoute = require('./routes/setlistRoute');
    var UserRoute = require('./routes/userRoute');

    // Initialise routes
    server.route(DiagnosticsRoute.endpoints);
    server.route(SetlistRoute.endpoints);
    server.route(UserRoute.endpoints);
});

// Start server
server.start((err) => {
    if (err) {
        throw err;
    }
    console.log('server running at: ', server.info.uri);
});