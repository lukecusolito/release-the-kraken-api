var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;

var UserSchema = new Schema({
    Username: { type: String, required: true, unique: true },
    Password: { type: String, required: true }
});

var User = Mongoose.model('User', UserSchema);

module.exports = User;