var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;

var SetlistSchema = new Schema({
    Title: { type: String, required: true, unique: true },
    Description: { type: String, required: true },
    //Setlist: { type: Array, required: true }
    Setlist: [
        {
            TrackName: { type: String, required: true },
            Artist: { type: String, required: true },
            Duration: { type: String, required: true }
        }
    ]
});

var Setlist = Mongoose.model('Setlist', SetlistSchema);

module.exports = Setlist;