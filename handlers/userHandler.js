var Mongoose = require('mongoose');
var Boom = require('boom');
var Crypto = require('crypto-js');
var nJwt = require('nJwt');
var User = require('../models/userModel');
const Config = require('../configs/config');

exports.Authenticate = (request, reply) => {
    User.findOne({
        'Username': request.payload.Username.toUpperCase(),
        'Password': request.payload.Password
    }, (err, user) => {
        if (user) {

            var claims = {
                sub: user.Username,
                iss: Config.server.jwtTokenIssuer,
                permissions: 'user'
            };

            var jwt = nJwt.create(claims, Config.server.jwtSecretKey);
            var token = jwt.compact();

            reply(token)
        } else {
            reply(Boom.badRequest("Authentication failed"));
        }
    });
}

exports.CreateUser = (request, reply) => {
    User.findOne({
        'Username': request.payload.Username
    }, (err, user) => {
        if (!user) {
            var newUser = new User(request.payload);
            newUser.Username = newUser.Username.toUpperCase();
            newUser.Password = Crypto.SHA512(newUser.Password).toString();

            newUser.save((err) => {
                if (!err) {
                    reply({
                        _id: newUser._id,
                        Created: true
                    }).created('/user/' + newUser._id)
                } else {
                    if (11000 === err.code || 11001 === err.code) {
                        reply(Boom.forbidden(err.message));
                    } else {
                        reply(Boom.forbidden(err));
                    }
                }
            });
        } else {
            reply(Boom.badRequest("Authentication failed"));
        }
    });
}