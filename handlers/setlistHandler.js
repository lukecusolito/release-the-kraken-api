var Mongoose = require('mongoose');
var Boom = require('boom');
var Setlist = require('../models/setlistModel');

exports.GetSetlists = (request, reply) => {
    Setlist.find({}, (err, setlists) => {
        if (err || !setlists) {
            reply(Boom.badRequest("Problem getting setlist", err));
            return;
        }
        reply(setlists);
    });
};

exports.CreateSetlist = (request, reply) => {
    var setlist = new Setlist(request.payload);

    setlist.save((err) => {
        if (!err) {
            reply({
                _id: setlist._id,
                Created: true
            }).created('/setlist/' + setlist._id)
        } else {
            if (11000 === err.code || 11001 === err.code) {
                reply(Boom.forbidden(err.message));
            } else {
                reply(Boom.forbidden(err));
            }
        }
    });
}

exports.DeleteSetlist = (request, reply) => {
    Setlist.remove({
        '_id': encodeURIComponent(request.params.id)
    }, (err) => {
        if (!err) {
            reply({ "Persisted": true });
        } else {
            reply(Boom.badRequest("Problem removing setlist", err));
        }
    });
}

exports.ModifySetlist = (request, reply) => {
    Setlist.findOne({
        '_id': encodeURIComponent(request.params.id)
    }, (err, setlist) => {
        if (err) {
            reply(Boom.badRequest("Setlist not found", err));
        } else if (setlist) {
            if (request.payload.Title)
                setlist.Title = request.payload.Title;
            if (request.payload.Description)
                setlist.Description = request.payload.Description;
            if (request.payload.Setlist)
                setlist.Setlist = request.payload.Setlist;

            setlist.save((err, setlist) => {
                if (!err) {
                    reply(setlist);
                } else {
                    reply(Boom.forbidden(err.message));
                }
            });
        } else {
            reply(Boom.forbidden("Setlist not found"));
        }
    });
}