var UserHandler = require('../handlers/userHandler');
var Joi = require('joi');

exports.endpoints = [
    {
        method: 'POST',
        path: '/api/user/authenticate',
        handler: UserHandler.Authenticate,
        config: {
            validate: {
                payload: Joi.object().keys({
                    Username: Joi.string().required(),
                    Password: Joi.string().required()
                }).options({ stripUnknown: true })
            },
            auth: false
        }
    }, {
        method: 'POST',
        path: '/api/user',
        handler: UserHandler.CreateUser,
        config: {
            validate: {
                payload: Joi.object().keys({
                    Username: Joi.string().required(),
                    Password: Joi.string().required()
                }).options({ stripUnknown: true })
            }
        }
    }
];