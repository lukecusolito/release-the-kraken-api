var SetlistHandler = require('../handlers/setlistHandler');
var Joi = require('joi');

exports.endpoints = [
    {
        method: 'GET',
        path: '/api/setlist',
        handler: SetlistHandler.GetSetlists
    },
    {
        method: 'POST',
        path: '/api/setlist',
        handler: SetlistHandler.CreateSetlist,
        config: {
            validate: {
                payload: Joi.object().keys({
                    Title: Joi.string().required(),
                    Description: Joi.string().required(),
                    Setlist: Joi.array().items(Joi.object({
                        TrackName: Joi.string().required(),
                        Artist: Joi.string().required(),
                        Duration: Joi.string().required()
                    })).required()
                }).options({ stripUnknown: true })
            }
        }
    },
    {
        method: 'DELETE',
        path: '/api/setlist/{id}',
        handler: SetlistHandler.DeleteSetlist,
        config: {
            validate: {
                params: {
                    id: Joi.string().required()
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/setlist/{id}',
        handler: SetlistHandler.ModifySetlist,
        config: {
            validate: {
                payload: Joi.object().keys({
                    Title: Joi.string().optional(),
                    Description: Joi.string().optional(),
                    Setlist: Joi.array().items(Joi.object({
                        TrackName: Joi.string().required(),
                        Artist: Joi.string().required(),
                        Duration: Joi.string().required()
                    })).optional()
                }).options({ stripUnknown: true }),
                params: {
                    id: Joi.string().required()
                }
            }
        }
    }
];