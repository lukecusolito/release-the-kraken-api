var DiagnosticsHandler = require('../handlers/diagnosticsHandler');

exports.endpoints = [
    {
        method: 'GET',
        path: '/api/diagnostics/isalive',
        handler: DiagnosticsHandler.IsAlive
    },
    {
        method: 'GET',
        path: '/api/diagnostics/version',
        handler: DiagnosticsHandler.GetVersion
    }
];